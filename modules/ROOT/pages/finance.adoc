= Financial Stuff

== Financial Management/Personal Finance
https://www.militaryonesource.mil/national-guard/financial-management/save-money-every-day/

== Personal Finance Articles
Personal Finance Articles from MilitaryOneSource
https://www.militaryonesource.mil/financial-legal/personal-finance/articles/page/2/

== Office of Financial Readiness
Allowances such as Basic Needs Allowance for junior Soldiers with dependents who qualify, Blended Retirement System (BRS), Education Benefits, Housing, Credit Fundamentals, Major Purchases, Pay and Savings, Tax Information, Retirement Savings, Inventing, Estate Planning, Financial Readiness, Insurance, Consumer Credit and Protections.
https://finred.usalearning.gov/

== Financial Front Line
Financial training broken up into milestones such as planning for deployment, first child, PCS, etc.
https://www.financialfrontline.org/

== Sen$e
Personal finance application for service members available on Google Play and the App Store.
https://www.militaryonesource.mil/resources/mobile-apps/sense/